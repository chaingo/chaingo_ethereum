require 'chaingo_ethereum/rinkeby/base'

module Rinkeby
  class Documents < Base
    ##
    # Creates a Document on the Blockchain
    # @param hash [String] the hash of the PDF
    #
    def create(hash)
      data = {
        hash: hash
      }
      @options[:body] = data.to_json
      self.class.post('/documents', @options)
    end

    ##
    # Verifys the contents of a Document on the Blockchain
    # @param hash [String] the hash of the PDF
    # @param transaction_hash [String] the transaction_hash returned by #create
    #
    def verify_document(hash, transaction_hash)
      data = {
        hash: hash,
        transactionHash: transaction_hash
      }
      @options[:query] = data.to_param
      self.class.get('/documents/verify', @options)
    end

    ##
    # Signs a Document on the Blockchain
    # @param hash [String] the hash of the PDF
    # @param signature [String] the text signature from the user
    #
    def sign(hash, signature)
      data = {
        hash: hash,
        signature: signature
      }
      @options[:body] = data.to_json
      self.class.post('/documents/signatures', @options)
    end

    ##
    # Verifys the contents of a Document on the Blockchain
    # @param hash [String] the hash of the PDF
    # @param signature [String] the text signature from the user
    # @param transaction_hash [String] the transaction_hash returned by #sign
    #
    def verify_signature(hash, signature, transaction_hash)
      data = {
        hash: hash,
        signature: signature,
        transactionHash: transaction_hash
      }
      @options[:query] = data.to_param
      self.class.get('/documents/signatures/verify', @options)
    end
  end
end
