require 'chaingo_ethereum/rinkeby/base'

module Rinkeby
  class Identifications < Base
    def create(hash)
      data = {
        hash: hash
      }
      @options[:body] = data.to_json
      self.class.post('/identifications', @options)
    end

    def verify(hash, transaction_hash)
      data = {
        hash: hash,
        transactionHash: transaction_hash
      }
      @options[:query] = data.to_param
      self.class.get('/identifications/verify', @options)
    end
  end
end
