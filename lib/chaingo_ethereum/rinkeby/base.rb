require 'chaingo_ethereum/rinkeby'
require 'active_record'
require 'httparty'

module Rinkeby
  class Base
    include HTTParty

    base_uri ENV.fetch('RINKEBY_API_URL', 'https://rinkeby.chaingotech.com/')

    def initialize
      @options = {
        body: {},
        headers: { 'Content-Type' => 'application/json' },
        timeout: 5
      }
    end
  end
end
