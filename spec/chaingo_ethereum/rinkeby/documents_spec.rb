require 'spec_helper'
require 'chaingo_ethereum/rinkeby/documents'

RSpec.describe Rinkeby::Documents, type: :lib do
  subject { instance }
  let(:instance) { described_class.new }

  describe '#create', :vcr do
    it 'creates on the blockchain' do
      response = instance.create('test1234')
      expect(response.success?).to be true
      json_response = JSON.parse(response.body, symbolize_names: true)
      expect(json_response[:status]).to eq('success')
      expect(json_response[:data][:result]).to eq('0xfd67f07d215cd136316ba06cd12ea38fcc48f31fe1adcdee58347cb8d8259045')
    end
  end

  describe '#verify_document', :vcr do
    it 'verifies data on the blockchain' do
      response = instance.verify_document(
        'test1234',
        '0xfd67f07d215cd136316ba06cd12ea38fcc48f31fe1adcdee58347cb8d8259045'
      )
      expect(response.success?).to be true
      json_response = JSON.parse(response.body, symbolize_names: true)
      expect(json_response[:status]).to eq('success')
      expect(json_response[:data][:result]).to eq(true)
    end
  end

  describe '#sign', :vcr do
    it 'creates a signature on the blockchain' do
      response = instance.sign('test1234', 'Mr. Test')
      expect(response.success?).to be true
      json_response = JSON.parse(response.body, symbolize_names: true)
      expect(json_response[:status]).to eq('success')
      expect(json_response[:data][:result]).to eq('0x9fcb9e3bc8b1d2f5c86d37e02d4d798acdb544e694a90ae82a9a63d60e782d7f')
    end
  end

  describe '#verify_signature', :vcr do
    it 'verifies a signature on the blockchain' do
      response = instance.verify_signature(
        'test1234',
        'Mr. Test',
        '0x9fcb9e3bc8b1d2f5c86d37e02d4d798acdb544e694a90ae82a9a63d60e782d7f'
      )
      expect(response.success?).to be true
      json_response = JSON.parse(response.body, symbolize_names: true)
      expect(json_response[:status]).to eq('success')
      expect(json_response[:data][:result]).to eq(true)
    end
  end
end
