require 'spec_helper'
require 'chaingo_ethereum/rinkeby/identifications'

RSpec.describe Rinkeby::Identifications, type: :lib do
  subject { instance }
  let(:instance) { described_class.new }

  describe '#create', :vcr do
    it 'creates on the blockchain' do
      response = instance.create('test1234')
      expect(response.success?).to be true
      json_response = JSON.parse(response.body, symbolize_names: true)
      expect(json_response[:status]).to eq('success')
      expect(json_response[:data][:result]).to eq('0x3200322318994bef5a8f496edb3e72d4f575f187dfec69f273390d85092d52b6')
    end
  end

  describe '#verify', :vcr do
    it 'verifies data on the blockchain' do
      response = instance.verify(
        'test1234',
        '0x3200322318994bef5a8f496edb3e72d4f575f187dfec69f273390d85092d52b6'
      )
      expect(response.success?).to be true
      json_response = JSON.parse(response.body, symbolize_names: true)
      expect(json_response[:status]).to eq('success')
      expect(json_response[:data][:result]).to eq(true)
    end
  end
end
